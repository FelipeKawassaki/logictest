package Solution3;

public class Chocolate {
	
	private int id;
	private boolean ate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isAte() {
		return ate;
	}
	public void setAte(boolean ate) {
		this.ate = ate;
	}

}
