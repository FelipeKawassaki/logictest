package Solution3;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class MainSolution3 {

	List<Chocolate> chocolateList = new ArrayList<Chocolate>();
	String result = "";

	public String solution3(int N, int M) {

		populateList(N);

		int size = chocolateList.size();
		Integer lastEaten = null;
		Integer nextToEat = 0;
		int i = 0;
		boolean keep = true;

		while (keep) {

			Chocolate choco = chocolateList.get(i);

			if (choco.isAte() && (i == nextToEat)) {
				keep = false;
				break;
			}

			if (i == nextToEat) {
				result += i + ", ";
				choco.setAte(true);
				lastEaten = i;

				if ((nextToEat + M) >= size) {
					nextToEat = (nextToEat + M) - size;
				} else {
					nextToEat = nextToEat + M;
				}
			}

			if (i < (size - 1))
				i++;
			else
				i = 0;

		}
		if (result != null && result.length() > 0 && result.charAt(result.length() - 2) == ',') {
			result = result.substring(0, result.length() - 2);
			return result;
		} else
			return result;

	}

	public void populateList(int N) {

		Chocolate chocolate;

		for (int i = 0; i < N; i++) {
			chocolate = new Chocolate();
			chocolate.setId(i);
			chocolateList.add(chocolate);
		}

	}

	public List<Chocolate> getChocolateList() {
		return chocolateList;
	}

	public void setChocolateList(List<Chocolate> chocolateList) {
		this.chocolateList = chocolateList;
	}

}
