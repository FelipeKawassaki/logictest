package Solution2;

public class MainSolution2 {

	public int solution2(int[] A) {

		int len = A.length;
		int count = 1;
		int tmp = 0;
	
		for (int i = 0; i < len; i++) {
			for (int j = i + 1; j < len; j++) {
				if (A[i] > A[j]) {
					tmp = A[i];
					A[i] = A[j];
					A[j] = tmp;
				}
			}
		}
	
		for (int i = 0; i < len - 1; i++) {
			if (A[i] != A[i + 1])
				count++;
		}
	
		if (len == 0)
			count = 0;
	
		return count;

	}
}
