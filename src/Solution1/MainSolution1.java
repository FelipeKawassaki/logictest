package Solution1;

public class MainSolution1 {

	public int solution1(int[] A) {

		boolean breakPattern = true;

		for (int i = 0; i < A.length; i++) {
			breakPattern = true;

			for (int j = 0; j < A.length; j++)
				if (A[i] == A[j] && i != j) { 
					breakPattern = false;
					break;
				}
			if (breakPattern)
				return A[i];

		}
		return 0;
	}

}
